# import spacy
from random import choice
from warnings import warn
from sys import stderr

__all__ = "State", "Traverser", \
          "like_family", "eg_family", "closer_family", \
          "eq", "extra", "func", "no_text", \
          "s"

class State:
    __slots__ = "name", "texts", "sfxs", "like", "edges"

    def __init__(self, name=None, *,
                 text=None, texts=None,
                 sfxs=None, like=None, edges=None):
        assert (text is None) or (texts is None), \
               "Don't provide text and texts."

        self.name = name

        if texts is None and text is not None:
            texts = text,
        self.texts = texts

        if sfxs is None:
            sfxs = []
        self.sfxs = sfxs

        self.like = like

        if edges is None:
            edges = []
        self.edges = edges

    def add_edge(self, match, state):
        self.edges.append((match, state))

    def add_side_effects(self, *sfxs):
        self.sfxs.extend(sfxs)

    def __repr__(self):
        if self.name is not None:
            return f"#{self.name}"
        try:
            return f"{self.__class__.__name__}" \
                   f"(texts={self.texts}, sfxs={self.sfxs}, " \
                   f"like={self.like}, edges={self.edges})"
        except RecursionError:
            # You know… just in case.
            return "..."

class Match(object):
    __slots__ = ()

    def __or__(self, other):
        if isinstance(other, OrMatch):
            return NotImplemented
        return OrMatch((self, other))

    def __and__(self, other):
        if isinstance(other, AndMatch):
            return NotImplemented
        return AndMatch((self, other))

##    def __mul__(self, other):
##        try:
##            other = float(other)
##        except TypeError:
##            return NotImplemented
##        return MulMatch(self, other)
##
##    def __rmul__(self, other):
##        return self * other

class OrMatch(tuple):
    __slots__ = ()

    def __or__(self, other):
        if isinstance(other, OrMatch):
            return type(self)((*self, *other))
        return type(self)((*self, other))

    def __ror__(self, other):
        if isinstance(other, OrMatch):
            return type(self)((*other, *self))
        return type(self)((other, *self))

    def __and__(self, other):
        if isinstance(other, AndMatch):
            return NotImplemented
        return AndMatch((self, other))

    def __repr__(self):
        return "(" + " | ".join(map(repr, self)) + ")"

    def __call__(self, doc, extra):
        return max(match(doc, extra) for match in self)

class AndMatch(tuple):
    __slots__ = ()

    def __and__(self, other):
        if isinstance(other, AndMatch):
            return type(self)((*self, *other))
        return type(self)((*self, other))

    def __rand__(self, other):
        if isinstance(other, AndMatch):
            return type(self)((*other, *self))
        return type(self)((other, *self))

    def __or__(self, other):
        if isinstance(other, OrMatch):
            return NotImplemented
        return OrMatch((self, other))

    def __repr__(self):
        return "(" + " & ".join(map(repr, self)) + ")"

    def __call__(self, doc, extra):
        return min(match(doc, extra) for match in self)

class LikeMatch(Match):
    __slots__ = "similarity", "threshold"

    def __init__(self, similarity, threshold):
        self.similarity = similarity
        self.threshold = threshold

    def __call__(self, doc, extra):
        if doc is None:
            return False
        globals()["fred"] = (self, doc)
        return max(self.similarity(doc) - self.threshold,
                   max(self.similarity(token) - self.threshold
                       for token in doc))

    def __repr__(self):
        return f"{self.__class__.__name__}" \
               f"({self.similarity}, {self.threshold})"

class ExampleMatch(Match):
    __slots__ = "similarity", "threshold"

    def __init__(self, similarity, threshold):
        self.similarity = similarity
        self.threshold = threshold

    def __call__(self, doc, extra):
        if doc is None:
            return False
        return self.similarity(doc) - self.threshold

    def __repr__(self):
        return f"{self.__class__.__name__}" \
               f"({self.similarity}, {self.threshold})"

class CloserMatch(Match):
    __slots__ = "sima", "simb"

    def __init__(self, sima, simb):
        self.sima = sima
        self.simb = simb

    def __call__(self, doc, extra):
        if doc is None:
            return False
        return self.sima(doc) - self.simb(doc)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.sima}, {self.simb})"

class ExtraMatch(Match):
    __slots__ = "key", "func"

    def __init__(self, key, func):
        self.key = key
        self.func = func

    def __call__(self, doc, extra):
        if extra is None:
            return False
        return self.func(extra[self.key])

    def __repr__(self):
        return f"{self.__class__.__name__}({self.key}, {self.func})"

class FuncMatch(Match):
    __slots__ = "f",

    def __init__(self, f):
        self.f = f

    def __call__(self, doc, extra):
        return self.f(doc, extra)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.f})"

family_similarity = lambda nlp, arg: (
    nlp(arg) if isinstance(arg, str)
    else nlp(arg[0])[arg[1]]
).similarity

def family_family(MatchType, name, threshold):
    def type_family(nlp):
        def like(*args, threshold=threshold):
            return OrMatch(
                MatchType(
                    family_similarity(nlp, arg),
                    threshold
                )
                for arg in args
            )
        # like.__name__ = f"like_{model_name}"
        # like.__name__ = f"like_{hex(id(nlp))}"
        return like
    type_family.__name__ = name
    type_family.__qualname__ = f"{__name__}.{name}"
    return type_family

# Pick a better default threshold
like_family = family_family(LikeMatch, "like_family", 0.80)
eg_family = family_family(ExampleMatch, "eg_family", 0.80)

def closer_family(nlp):
    def closer(a, b):
        return CloserMatch(
            family_similarity(nlp, a),
            family_similarity(nlp, b)
        )
    return closer

def eq(key, x):
    return ExtraMatch(key, lambda v: (-1, 1)[x == v])

def extra(key, func):
    return ExtraMatch(key, func)

def func(f):
    return FuncMatch(f)

class NoTextMatch(Match):
    __slots__ = ()
    
    def __call__(self, doc, extra):
        return (-1, 1)[doc is None]

    def __repr__(self):
        return "no_text"

no_text = NoTextMatch()

class S:
    """A singleton for convenient production of slices."""

    __slots__ = ()

    def __getitem__(self, index):
        return index

s = S()

class Traverser:
    __slots__ = "nlp", "state", "errors", "extra"

    DEBUG = False

    def __init__(self, nlp, start, errors=("Please reword.",), *, extra=None):
        self.nlp = nlp
        self.state = start
        self.errors = errors
        self.extra = extra

    def start(self):
        if self.extra is not None:
            raise RuntimeError("start() must be called exactly once.")
        self.extra = {}
        return self._do_effects(None)

    def _do_effects(self, doc):
        current = self.state
        while current is not None:
            for sfx in current.sfxs:
                sfx(doc, self.extra)
            current = current.like
        if self.state.texts is None:
            return None
        return choice(self.state.texts)
            
    def _do_edge(self, doc):
        current = self.state
        weights = []
        while current is not None:
##            if self.DEBUG:
##                l = [(match, state)
##                     for (match, state) in current.edges
##                     if match(doc, self.extra)]
##                if len(l) != 1:
##                    warn(f"len({l}) != 1", RuntimeWarning)
            
            weights.extend(
                filter(
                    lambda x: x[0] > 0,
                    (
                        (match(doc, self.extra), state)
                        for match, state in current.edges
                    )
                )
            )
            current = current.like
        if self.DEBUG:
            print("  candidates:", weights)
        if weights:
            self.state = max(weights, key=lambda x: x[0])[1]
            if self.DEBUG:
                print("->", self.state, file=stderr)
            return True
        return False

    def _edge_effects(self, doc):
        if not self._do_edge(doc):
            return choice(self.errors)
        return self._do_effects(doc)

    def text_interaction(self, text):
        doc = self.nlp(text)
        return self._edge_effects(doc)

    def extra_interaction(self, extra):
        self.extra.update(extra)
        return self._edge_effects(None)
