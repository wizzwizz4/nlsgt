# Example!

import spacy
from . import *  # from nlsgt import *

Traverser.DEBUG = True

nlp = spacy.load("en_core_web_md")
like = like_family(nlp)
eg = eg_family(nlp)
closer = closer_family(nlp)

def initialise_extra(doc, extra):
    extra["idc_reached"] = extra.get("idc_reached", False)
    extra["ctrl-c"] = False

start = State("start", text="Welcome!", sfxs=[initialise_extra])
restart = State("restart", texts=("Restarting.",
                                  "Back to the beginning, then? Okay.",
                                  "GOTO 10"), like=start)
bricks = State("bricks",
               texts=("How did you guess?", "Did you look at the source?"))
idc = State("idc", text="I don't actually care. I'm not a real person.")
win = State("win", text="You win!")
debug = State(sfxs=[print], like=idc)

def idc_side_effect(doc, extra):
    extra["idc_reached"] = True

restart_condition = like("restart")
for state in (bricks, idc, win):
    state.add_edge(restart_condition, restart)

start.add_edge(like("mortar") & like("bricks and cement"), bricks)

bricks.add_edge(eq("idc_reached", True) & eq("ctrl-c", True), win)
bricks.add_edge(eq("idc_reached", False) | eq("ctrl-c", False), idc)

idc.add_side_effects(idc_side_effect)
idc.add_edge(
    like(
        "debug",
        "debugger",
        ("Debugging is fun.", 0),
        ("software bug removal tool", s[1:3]),
        ("I like bug removing.", s[2:]),
        threshold=0.9
    )
        | eg("Show me the stats.", "Access debug menu.")
          & closer("please", "do it now"),
    debug
)

win.add_edge(like("42"), debug)

t = Traverser(nlp, start, errors=("???", "That's not right."))
x = t.start()
if x is not None:
    print(x)
while True:
    try:
        i = input("> ")
    except KeyboardInterrupt:
        o = t.extra_interaction({"ctrl-c": True})
    else:
        o = t.text_interaction(i)
    if o is not None:
        print(o)
