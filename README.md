# nlsgt

A natural language stateful graph traversal system, based on spaCy. Useful in chat bots and CYoA games.

## Installation instructions

These instructions will let you run the demo. This isn't a proper Python package yet, so instructions are a little convoluted.

 1. Have a copy of this git repository.
 2. Install `pip` and `virtualenv`:
    
    ```bash
    # Do not run these commands if you have a package manager.
    sudo python3 -m ensurepip
    sudo python3 -m pip install --upgrade pip
    
    # Run this one, though.
    sudo python3 -m pip install virtualenv
    ```
    
 3. `cd` to this repo.
 4. Run `python3 -m virtualenv .env` to create the virtualenv.
 5. Run `source .env/bin/activate` to enter the virtualenv. (`deactivate` will leave the virtualenv.)
 6. Run `pip install -r requirements.txt` to install the dependencies.
 7. Run `python -m spacy download en_core_web_md` to install the other dependency.

You should now be able to launch the demo with `python -m nlsgt`.

### Installation as module
As above, but use your code's directory in step 3 – write the full path to `nlsgt`'s `requirements.txt` in step 6. Then copy the `nlsgt` directory (containing `__init__.py`) into `.env/lib/python3.x/site-packages` as step 8. If you're using git, add `.env` into your `.gitignore` (if it's not there already); you don't need to commit your `virtualenv` directory.
